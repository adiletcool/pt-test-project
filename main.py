from fastapi import FastAPI

from app.routers import cidr
import uvicorn

app = FastAPI(title="Test")

app.include_router(cidr)


def main():
    uvicorn.run(
        'main:app',
        host='0.0.0.0',
        port=8000,
        workers=2,
    )


if __name__ == '__main__':
    main()
