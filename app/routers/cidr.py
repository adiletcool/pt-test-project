from fastapi import APIRouter, Depends
from fastapi.responses import FileResponse

from app.utils.request_parameters import QueryCountryCode

router = APIRouter(
    prefix="/cidr",
    tags=["CIDR"],
)


@router.get("")
async def get_cidr(
        parameters=Depends(QueryCountryCode)
):
    country_code = parameters.country_code
    return FileResponse(
        path=f'./cidr_output/{country_code}.txt',
        filename=f'{country_code}.txt',
        media_type='multipart/form-data'
    )
