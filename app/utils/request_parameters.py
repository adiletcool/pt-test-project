from dataclasses import dataclass


@dataclass
class QueryCountryCode:
    country_code: str
