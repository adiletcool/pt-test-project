import argparse
import ipaddress
from multiprocessing import Pool

import netaddr
import pandas as pd

N_PROCESSES = 4


def parse_args():
    cli = argparse.ArgumentParser()
    cli.add_argument('--filename', '-f', type=str, default='country.csv')
    cli.add_argument('--output', '-o', type=str, default='cidr_output')
    return cli.parse_args()


def parse_country(country: str):
    df_country = df[df.country == country].copy()
    df_country['ip_version'] = df_country.start_ip.apply(lambda x: ipaddress.ip_address(x).version)
    df_country = df_country[df_country.ip_version == 4]
    cidr = df_country.apply(lambda x: netaddr.iprange_to_cidrs(x.start_ip, x.end_ip), axis=1)
    res = cidr.explode().astype(str)
    res.to_csv(f'{args.output}/{country}.txt', header=False, index=False)

    # res = map(str, netaddr.cidr_merge(res))
    # res = '\n'.join(res)
    # with open(f'{args.output}/{country}.txt', 'w') as f:
    #     f.writelines(res)


if __name__ == '__main__':
    args = parse_args()
    df = pd.read_csv(args.filename)
    df = df[~df.country.isna()][['start_ip', 'end_ip', 'country']]
    countries = df.country.unique()

    with Pool(N_PROCESSES) as pool:
        pool.map(parse_country, countries)
