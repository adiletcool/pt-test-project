# Part 1
## Generate CIDR for countries
```
gzip -dk country.csv.gz 
python process_cidr.py --filename country.csv --output cidr_output
```


# Part 2
## Run FastAPI app in Docker
```
docker build -t myimage .
docker run -d --name mycontainer -p 8000:8000 -t myimage
```

Test request
```
curl -o RU.txt http://127.0.0.1:8000/cidr?country_code=RU
```