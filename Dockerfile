FROM python:3.11-alpine

WORKDIR /code

COPY ./main.py /code/main.py

COPY ./app /code/app
COPY ./cidr_output /code/cidr_output
COPY ./requirements.txt /code/requirements.txt

RUN pip install -r /code/requirements.txt

EXPOSE 8000
CMD ["python", "main.py"]